---
layout: markdown_page
title: "Contributing to UX Design"
description: "These instructions are for those wanting to contribute UX designs specifically. The UX department at GitLab uses Sketch for all of its designs."
canonical_path: "/community/contribute/ux-design/"
---

## UX Design

These instructions are specifically for those wanting to contribute UX designs. 

The UX department at GitLab uses [Figma](https://www.figma.com/) for all of its designs, and you can see our [Design Repository documentation](https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md#getting-started) for details on working with our files. 

Also, visit our [Contributing guidelines](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/development/contributing/index.md) for general guidance on contributing. While they are code-focused instructions, they will help you understand the overall process of contributing.

1. You may leverage the [Pajamas UI Kit](https://www.figma.com/community/file/781156790581391771) in Figma to create mockups for your proposals.
1. You do not need to use Figma to contribute. We will gladly accept handmade drawings and sketches, wireframes, manipulated DOM screenshots, prototypes, and so on.
1. You can find design pattern documentation in our [Design System](https://design.gitlab.com/). Use it to understand where and when to use common design solutions.
1. If you don't already have an idea for a UX improvement, you can pick an existing problem to work on from [this list of issues looking for community contributions](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=UX&label_name[]=Accepting%20merge%20requests&sort=weight)
1. Be sure to comment and verify that no one else is working on the UX for the issue, and to make sure we’re still interested in a given contribution.
1. Ask questions to clarify the problem being solved and make your UX suggestions using words, low-fi wireframes, hi-fi designs, or even prototypes. Ask for help if you’re new or if you get stuck. We’re happy to help! You can ping the UX Department in an issue or comment using this handle `@gitlab-com/gitlab-ux`.
1. Ask for a review from a GitLab Product Designer. You’ll likely need to change some things once the reviewer has completed their review. You may also require multiple reviews depending on the scope of the UX.
1. Get your UX approved!

---
layout: handbook-page-toc
title: "GitLab InnerSourcing Hands On Guide- Lab 5"
description: "This Hands On Guide Lab is designed to walk you through the lab exercises used in the GitLab InnerSourcing course."
---
# GitLab InnerSourcing Hands On Guide- Lab 5
{:.no_toc}

## LAB 5- FORK A PROJECT AND CONTRIBUTE TO IT
### Create a Forked Project
1. In the GitLab Demo Cloud, locate your {Last name}InnerSourcing Project and click on it.  
2. Locate the **Fork** button and click on it. 
3. Locate your training user group in the available name spaces/groups and click the green **Select** button.

### Make Changes to the project
1. Click on the README.md file to open it.
2. In the upper-right hand corner, click the **Open in Web IDE** button.
3. In the editor, and add a new description for the project. 
4. Click the blue **Commit** button on the bottom of the screen. 
5. In the commit message box, type “***Updated the README.md file***” and then click the **Commit** button. 

### Verify Changes in a Merge Request
1. On the very bottom of your browser window, locate the merge request message and the small exclamation point- Click on the blue hyperlinked number to open the request.
2. On the Merge Request window- locate the Assignee section in the upper right-hand corner. Ensure the Merge Request is assigned to yourself. 
3. On your Merge Request, click on the **Changes** tab directly below the Merge Request title.  
4. Click on **Changes** 
5. Hover over the left side of any line and a comment icon will appear. Hover over line 3 and click the **comment** icon. 
6. In the comment field type, “@kmarquart this project will replace project XYZ with close requirements please review!” and click the **Start a review** button. Then click the **Submit Review** button.
7. To mark that the comment has been looked at and the code adjusted, click the **Resolve Thread** icon to close the review.  

### Approve the Merge Request
1. To mark the Merge Request ready, click the **Mark as ready** button in the upper right hand corner.  
2. Click the **Overview** tab on the Merge Request.  
3. Click the **Merge** button and ensure the Delete source branch checkbox is enabled. 
4. If there were eligible approvers, the approval button would be in the View Eligible Approvers section.  


### SUGGESTIONS?

If you wish to make a change to our Hands on Guide for GitLab InnerSourcing please submit your changes via Merge Request!
